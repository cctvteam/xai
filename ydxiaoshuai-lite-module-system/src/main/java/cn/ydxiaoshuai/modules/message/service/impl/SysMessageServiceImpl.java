package cn.ydxiaoshuai.modules.message.service.impl;

import cn.ydxiaoshuai.common.system.base.service.impl.JeecgServiceImpl;
import cn.ydxiaoshuai.modules.message.entity.SysMessage;
import cn.ydxiaoshuai.modules.message.mapper.SysMessageMapper;
import cn.ydxiaoshuai.modules.message.service.ISysMessageService;
import org.springframework.stereotype.Service;

/**
 * @Description: 消息
 * @Author: 小帅丶
 * @Date:  2019-04-09
 * @Version: V1.0
 */
@Service
public class SysMessageServiceImpl extends JeecgServiceImpl<SysMessageMapper, SysMessage> implements ISysMessageService {

}
