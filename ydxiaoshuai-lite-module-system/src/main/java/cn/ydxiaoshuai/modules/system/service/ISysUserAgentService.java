package cn.ydxiaoshuai.modules.system.service;

import cn.ydxiaoshuai.modules.system.entity.SysUserAgent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 用户代理人设置
 * @Author: 小帅丶
 * @Date:  2019-04-17
 * @Version: V1.0
 */
public interface ISysUserAgentService extends IService<SysUserAgent> {

}
