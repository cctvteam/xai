package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className TouTiaoAccessTokenBean
 * @Description 头条AccessToken
 * @Date 2020/6/17-16:22
 **/
@NoArgsConstructor
@Data
public class TouTiaoAccessTokenBean {

    private String access_token;
    private int expires_in;
    //可能是全局头
    private long errcode;
    private String errmsg;
    private long error;
    private String message;
}
