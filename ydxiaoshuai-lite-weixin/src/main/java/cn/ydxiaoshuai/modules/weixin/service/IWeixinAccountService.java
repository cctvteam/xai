package cn.ydxiaoshuai.modules.weixin.service;

import cn.ydxiaoshuai.modules.weixin.entity.WeixinAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信公众账号表
 * @Author: 小帅丶
 * @Date:   2020-09-10
 * @Version: V1.0
 */
public interface IWeixinAccountService extends IService<WeixinAccount> {
    public void refreshOneAppid(String account_code);
}
