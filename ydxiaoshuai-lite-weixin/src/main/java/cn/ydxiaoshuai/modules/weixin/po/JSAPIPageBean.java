package cn.ydxiaoshuai.modules.weixin.po;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @Description JSAPI
 * @author 小帅丶
 * @className JSAPIPageBean
 * @Date 2019/12/2-13:56
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JSAPIPageBean {
    /** 微信APPID */
    private String appId;
    /** 通过 getticket 接口获取的 JSAPI 调用凭证 */
    private String jsapi_ticket;
    /** 随机生成的字符串 由于JS此参数为驼峰 所以这里也遵循了JS参数的大小写规范 */
    private String nonceStr;
    /** 当前时间戳 */
    private String timestamp;
    /** 签名值 */
    private String signature;
    /** 访问的页面路径 */
    private String url;
}
