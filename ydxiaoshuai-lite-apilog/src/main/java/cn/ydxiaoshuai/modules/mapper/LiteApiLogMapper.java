package cn.ydxiaoshuai.modules.mapper;

import cn.ydxiaoshuai.modules.entity.LiteApiLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
public interface LiteApiLogMapper extends BaseMapper<LiteApiLog> {

}
