package cn.ydxiaoshuai.modules.faceeffects.service.impl;

import cn.ydxiaoshuai.modules.faceeffects.entity.FaceEffectsMergeTemplate;
import cn.ydxiaoshuai.modules.faceeffects.mapper.FaceEffectsMergeTemplateMapper;
import cn.ydxiaoshuai.modules.faceeffects.service.IFaceEffectsMergeTemplateService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 人脸融合模板图表
 * @Author: 小帅丶
 * @Date:   2020-09-04
 * @Version: V1.0
 */
@Service
public class FaceEffectsMergeTemplateServiceImpl extends ServiceImpl<FaceEffectsMergeTemplateMapper, FaceEffectsMergeTemplate> implements IFaceEffectsMergeTemplateService {

}
